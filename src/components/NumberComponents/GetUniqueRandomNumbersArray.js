function GetUniqueRandomNumbersArray(){
    const set = new Set();
    while(set.size < 5) {
        set.add(Math.floor(5 + Math.random() * (36 + 1 - 5)))
    }
    let array = Array.from(set);
    return array.sort(function (a,b) {
        return a-b;
    });
}

export default GetUniqueRandomNumbersArray;