import './App.css';
import React, {Component} from 'react';
import Number from "./components/NumberComponents/Number";
import GetUniqueRandomNumbersArray from './components/NumberComponents/GetUniqueRandomNumbersArray';


class App extends Component {
    array = GetUniqueRandomNumbersArray();
    state = {
        numbers: [
            {number: this.array[0]},
            {number: this.array[1]},
            {number: this.array[2]},
            {number: this.array[3]},
            {number: this.array[4]},
        ]
    };

    changeNumbers = () => {
        let newArray = GetUniqueRandomNumbersArray();
        this.setState({
            numbers: [
                {number: newArray[0]},
                {number: newArray[1]},
                {number: newArray[2]},
                {number: newArray[3]},
                {number: newArray[4]},
            ]
        });

    };

    render() {
        return (
            <div className="App">
                <div>
                    <button onClick={this.changeNumbers}>New numbers</button>
                </div>
                <Number number={this.state.numbers[0].number}/>
                <Number number={this.state.numbers[1].number}/>
                <Number number={this.state.numbers[2].number}/>
                <Number number={this.state.numbers[3].number}/>
                <Number number={this.state.numbers[4].number}/>
            </div>
        );
    };
}

export default App;